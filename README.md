# Sublime Text 3

* [Download](http://www.sublimetext.com/3)
* [Packages](https://sublime.wbond.net/)

## Install on Windows

![Windows](https://bytebucket.org/magnobiet/sublime-text/raw/4dcd8dbdf134d57ba3f99d4abf778e3c3fe42221/Images/install-windows.gif)

Open **PowerShell** by right-clicking and selecting **Run as Administrator**.

```
cd "C:\Program Files\Sublime Text 3\"
git clone https://<username>@bitbucket.org/magnobiet/sublime-text.git Data
npm run install
```

## Install on Ubuntu

```
cd $HOME/Downloads
wget http://c758482.r82.cf2.rackcdn.com/sublime-text_build-3065_amd64.deb
sudo dpkg -i sublime-text_build-*.deb
cd $HOME/.config/
git clone git@bitbucket.org:magnobiet/sublime-text.git sublime-text-3
sudo npm run install
```

## Installed Packages
* AngularJS
* ApacheConf.tmLanguage
* ASCII Decorator
* AutoFileName
* Autoprefixer
* Bower
* BracketHighlighter
* BufferScroll
* Can I Use
* cdnjs
* Color Highlighter
* CSScomb JS
* DocBlockr
* Dotfiles Syntax Highlighting
* EditorConfig
* Emmet
* GitGutter
* Grunt
* HTML5
* JavaScript Console
* JavaScript Patterns
* JavaScript Snippets
* JsFormat
* LESS
* LineEndings
* List stylesheet variables
* Markdown Preview
* Nettuts+ Fetch
* PHP Getters and Setters
* PlainTasks
* Sass
* SassBeautify
* SCSS
* SideBarEnhancements
* StringEncode
* SublimeLinter
* SublimeLinter-contrib-scss-lint
* SublimeLinter-csslint
* SublimeLinter-jscs
* SublimeLinter-jshint
* SublimeLinter-json
* SublimeLinter-php
* SublimeLinter-pylint
* TrailingSpaces
* Trimmer
* Vagrant
* YUI Compressor

## Icon

![Sublime Text Icon](https://bytebucket.org/magnobiet/sublime-text/raw/d09deaf6216d35f9a252dd02157f77bfe4a424cf/Icons/sublime-text.png)

by [Jon-Paul Lunney](https://dribbble.com/shots/382465-Sublime-Text-2-update-Replacement-Icon)
